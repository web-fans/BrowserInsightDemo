#BrowserInsightDemo

前端常用工具调研-OneApm Browser Insight 介绍前端工具 整理常用的前端工具 当然也推荐大家在测试环境、灰度发布环境、正式生产环境和端到端的测试中使用 前端常用工具调研-OneApm Browser Insight http://www.oneapm.com/bi/feature.html

基于 nodejs 与npm 的前端项目构建工具，前端开发必备工具

Grunt 相关插件 grunt-contrib-concat js，css合并 grunt-contrib-uglify js压缩 grunt-contrib-mincss css 文件压缩

glup 相关插件 gulp-concat 
gulp-uglify gulp-minify-css js 模块化相关工具 sea.js equire.js browserify webpack bower

css 模块化相关工具 sass less

测试相关工具 Karma Phantomjs SELENIUM 页面结构分析 YSlow PageSpeed
